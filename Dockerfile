FROM docker:stable

RUN apk add --update --no-cache python3 curl tar gzip && \
    mkdir /usr/local/gc/ && \
    curl https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz | tar  -C /usr/local/gc/ -xz && /usr/local/gc/google-cloud-sdk/install.sh

ENV PATH $PATH:/usr/local/gc/google-cloud-sdk/bin
